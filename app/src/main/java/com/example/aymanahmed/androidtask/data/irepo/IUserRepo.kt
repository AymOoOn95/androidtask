package com.example.aymanahmed.androidtask.data.irepo

import com.example.aymanahmed.androidtask.data.model.User
import com.example.aymanahmed.androidtask.data.response.LoginResponse
import io.reactivex.Observable

interface IUserRepo {
    fun login(user: User): Observable<LoginResponse>
    fun saveUser(user: User)
}