package com.example.aymanahmed.androidtask.data.model

class AppLocation(val long: Double, val lat: Double){

    override fun toString(): String {
        return "long is $long - late is $lat"
    }
}