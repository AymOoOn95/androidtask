package com.example.aymanahmed.androidtask.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.example.aymanahmed.androidtask.util.SHARED_NAME
import dagger.Module
import dagger.Provides

@Module
class AppModule(val application: Application) {

    @Provides
    fun providesAppSharedPref(): SharedPreferences {
        return application.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE)
    }
}