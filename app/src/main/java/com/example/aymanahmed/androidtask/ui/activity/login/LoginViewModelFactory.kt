package com.example.aymanahmed.androidtask.ui.activity.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.aymanahmed.androidtask.data.irepo.IUserRepo

class LoginViewModelFactory(val userRepo: IUserRepo) : ViewModelProvider.Factory {


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LoginViewModel(userRepo) as T
    }
}