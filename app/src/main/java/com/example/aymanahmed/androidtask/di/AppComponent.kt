package com.example.aymanahmed.androidtask.di

import com.example.aymanahmed.androidtask.ui.activity.login.LoginActivity
import dagger.Component


@Component(modules = [AppModule::class, RepoModule::class])
interface AppComponent {
    fun inject(loginActivity: LoginActivity)
}