package com.example.aymanahmed.androidtask.ui.activity.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.lifecycle.ViewModelProviders
import com.example.aymanahmed.androidtask.R
import com.example.aymanahmed.androidtask.data.irepo.IUserRepo
import com.example.aymanahmed.androidtask.data.model.User
import com.example.aymanahmed.androidtask.ui.activity.main.MainActivity
import com.example.aymanahmed.androidtask.util.MyApplication
import com.example.aymanahmed.androidtask.util.makeToast
import com.google.android.material.textfield.TextInputEditText
import javax.inject.Inject

class LoginActivity : AppCompatActivity() {


    @Inject
    lateinit var userRepo: IUserRepo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        (application as MyApplication).appComponent?.inject(this)
        val viewModel: LoginViewModel =
            ViewModelProviders.of(this, LoginViewModelFactory(userRepo)).get(LoginViewModel::class.java)
        val username = findViewById<EditText>(R.id.login_username)
        val password = findViewById<EditText>(R.id.login_password)
        // todo check if there is logged user or not
        findViewById<Button>(R.id.login_btn).setOnClickListener {
            username.text?.toString()?.let { usernameStr ->
                password.text?.toString()?.let { passwordStr ->
                    var user = User(usernameStr, passwordStr)
                    viewModel.userLogin(user).subscribe({
                        if (it.status) {
                            viewModel.saveUser(user)
                            this.makeToast("Login successfully")
                            startActivity(Intent(this, MainActivity::class.java))
                        } else {
                            this.makeToast("Wrong username or password")
                        }
                    }, {
                        this.makeToast("Connection error")
                    })

                } ?: this.makeToast("Please type password")
            } ?: this.makeToast("Please type username")
        }
    }
}
