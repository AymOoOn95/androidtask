package com.example.aymanahmed.androidtask.data.repo

import android.content.SharedPreferences
import androidx.core.content.edit
import com.example.aymanahmed.androidtask.data.irepo.IUserRepo
import com.example.aymanahmed.androidtask.data.model.User
import com.example.aymanahmed.androidtask.data.response.LoginResponse
import com.example.aymanahmed.androidtask.util.PASSWORD
import com.example.aymanahmed.androidtask.util.PASSWORD_KEY
import com.example.aymanahmed.androidtask.util.USERNAME
import com.example.aymanahmed.androidtask.util.USERNAME_KEY
import io.reactivex.Observable

class UserRepo(val appSharedPref: SharedPreferences) : IUserRepo {

    override fun login(user: User): Observable<LoginResponse> {
        return if (user.username == USERNAME && user.password == PASSWORD) {
            Observable.just(LoginResponse(user, true))
        } else {
            Observable.just(LoginResponse(null, false))
        }
    }


    override fun saveUser(user: User) {
        appSharedPref.edit {
            putString(USERNAME_KEY, user.username)
            putString(PASSWORD_KEY, user.password)
        }
    }
}