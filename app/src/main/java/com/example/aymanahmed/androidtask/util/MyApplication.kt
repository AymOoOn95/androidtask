package com.example.aymanahmed.androidtask.util

import android.app.Application
import com.example.aymanahmed.androidtask.di.AppComponent
import com.example.aymanahmed.androidtask.di.AppModule
import com.example.aymanahmed.androidtask.di.DaggerAppComponent
import com.example.aymanahmed.androidtask.di.RepoModule

class MyApplication : Application() {

    var appComponent: AppComponent? = null
    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).repoModule(RepoModule()).build()
    }
}