package com.example.aymanahmed.androidtask.data.response

import com.example.aymanahmed.androidtask.data.model.User

class LoginResponse(val user: User?, val status: Boolean)