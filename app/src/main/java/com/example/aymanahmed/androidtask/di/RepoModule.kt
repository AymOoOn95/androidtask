package com.example.aymanahmed.androidtask.di

import android.content.SharedPreferences
import com.example.aymanahmed.androidtask.data.irepo.IUserRepo
import com.example.aymanahmed.androidtask.data.repo.UserRepo
import dagger.Module
import dagger.Provides

@Module
class RepoModule {

    @Provides
    fun ProvidesUserRepo(appSharedPref: SharedPreferences): IUserRepo {
        return UserRepo(appSharedPref)
    }
}