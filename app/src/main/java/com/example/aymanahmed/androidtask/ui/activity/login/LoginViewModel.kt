package com.example.aymanahmed.androidtask.ui.activity.login

import androidx.lifecycle.ViewModel
import com.example.aymanahmed.androidtask.data.irepo.IUserRepo
import com.example.aymanahmed.androidtask.data.model.User
import com.example.aymanahmed.androidtask.data.repo.UserRepo
import com.example.aymanahmed.androidtask.data.response.LoginResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class LoginViewModel(private val userRepo: IUserRepo) : ViewModel() {

    fun userLogin(user: User): Observable<LoginResponse> {
        return userRepo.login(user).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }


    fun saveUser(user: User) {
        userRepo.saveUser(user)
    }
}